(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('vuedraggable'), require('v-mask')) :
    typeof define === 'function' && define.amd ? define(['exports', 'vuedraggable', 'v-mask'], factory) :
    (global = global || self, factory(global.jobbsontable = {}, global.draggable, global.vMask));
}(this, function (exports, draggable, vMask) { 'use strict';

    draggable = draggable && draggable.hasOwnProperty('default') ? draggable['default'] : draggable;

    //

    var script = {
        props: {
            value: {
                type: [Array, Object],
                required: true
            },
            dragAndDrop:{
                type: Boolean,
                required: false,
                default: false
            },
            noDragAndDropByClassName:{
                type: String,
                required: false,
                default: ''
            },
            dragEvent: {
                type: Function,
                required: false,
                default: function () {}
            },
            dropEvent: {
                type: Function,
                required: false,
                default: function () {}
            },
            idJobbsOnTable: {
                type: String,
                default: function () {
                    return Math.random().toString().substr(-Math.random().toString()+3);
                } 
            },
            styleFocus: {
                type: String,
                default: 'box-sizing:border-box; display:none; position:absolute;'
            }
        },      
        directives: {
            'mask' : vMask.VueMaskDirective
        },
        components: {
            draggable: draggable
        },
        computed: {
            dragOptions: function dragOptions() {
                return {
                    animation: 200,
                    ghostClass: "ghost",
                    easing: "cubic-bezier(0.19, 1, 0.22, 1)",
                    filter: this.noDragAndDropByClassName,
                    disabled: !this.dragAndDrop
                };
            },
        },
        data: function data() {
            return {
                inputValue: '',
                mascaraInput: '',
                dataWatch: [],
                maiorTr: 0,
                enterIsPress: false,
                areAmoviment: false,
                triggerInput: false,
                lineBefore: null,
                cellActived: {
                    indexColumn: null,
                    indexLine: null,
                    eventInput: null,
                    dataInput: null,
                    lastMoviment: null
                }
            }
        },
        methods: {
            onCellFocus: function onCellFocus(event, campo, indexLine, indexColumn){
                try{

                    var self = this;
                    this.enterIsPress = false;
                    this.cellActived.eventInput  = event.target;
                    this.cellActived.indexColumn = indexColumn;
                    this.cellActived.indexLine   = indexLine;
                    this.cellActived.dataInput   = campo;
                    this.triggerInput            = false;
                    this.inputValue              = null;

                    if(!this.areAmoviment){
                        if(typeof this.cellActived.dataInput.event == 'object' && typeof this.cellActived.dataInput.event.onClick == 'function')
                            { this.cellActived.dataInput.event.onClick(campo, indexLine, indexColumn); } // TRIGGER EVENT CLICK
                    }
                    this.areAmoviment = false;

                    this.adjustField();

                    switch(campo.atribute.type){
                        case 'input':  
                            this.inputValue = event.target.innerHTML;
                            setTimeout(function(){ self.inputValue = event.target.innerHTML; }, 30);
                            break;
                        case 'select': 
                            var htmlOption = '';
                            var selectFocus = document.getElementById("selectCellFocus"+this.idJobbsOnTable);
                            campo.atribute.options.forEach(function(option){ 
                                htmlOption += '<option value="'+option.value+'" >'+option.label+'</option>'; 
                            });

                            selectFocus.innerHTML = htmlOption;
                            this.inputValue       = campo.value; 
                            document.getElementById("selectCellFocus"+this.idJobbsOnTable).value = campo.value; 

                            break;
                        case 'money':
                            this.inputValue = event.target.innerHTML;
                            break;
                        case 'autocomplete':
                            this.inputValue = event.target.innerHTML;
                            break;
                    }

                }catch(err){
                    if(this.cellActived.lastMoviment != null){
                        this.moveCell(this.cellActived.lastMoviment);
                        this.cellActived.lastMoviment = null;
                    }
                }
            },
            adjustField: function adjustField(){
                var tableJobbsOnTable = document.getElementsByClassName("tableId"+this.idJobbsOnTable)[0];
                var inputFocus        = document.getElementById("inputCellFocus"+this.idJobbsOnTable);
                var datalist          = document.getElementById("datalist"+this.idJobbsOnTable);
                var selectFocus       = document.getElementById("selectCellFocus"+this.idJobbsOnTable);
                inputFocus.setAttribute("list", '');
                inputFocus.setAttribute("style", this.styleFocus);
                selectFocus.setAttribute("style", this.styleFocus);

                // DISABLED 
                try{ 
                    if(this.cellActived.dataInput.atribute.disabled){
                        inputFocus.classList.add("hiddenCellFocus");
                        selectFocus.classList.add("hiddenCellFocus");
                    } else {
                        throw true; 
                    }
                }catch(err){
                    inputFocus.classList.remove("hiddenCellFocus");
                    selectFocus.classList.remove("hiddenCellFocus");
                }

                // PUT THE MASK
                try{
                    if(this.cellActived.dataInput.atribute.mask.length > 0){
                        this.mascaraInput = this.cellActived.dataInput.atribute.mask;
                    }else{
                        throw true; 
                    }
                }catch(err){
                    this.mascaraInput = '';
                }

                // RESIZE
                var position = this.cellActived.eventInput.getBoundingClientRect();
                var left     = tableJobbsOnTable.offsetLeft + this.cellActived.eventInput.offsetLeft;
                var top      = tableJobbsOnTable.offsetTop + this.cellActived.eventInput.offsetTop;
                var style    = "box-sizing: border-box; display: inline-block; position: absolute; top: "+top+"px; left:"+left+"px; width: "+position.width+"px; height:"+position.height+"px;"+this.cellActived.dataInput.style;

                switch(this.cellActived.dataInput.atribute.type){
                    case 'input':  
                        inputFocus.setAttribute("style", style);
                        inputFocus.focus();
                        break;
                    case 'select': 
                        selectFocus.setAttribute("style", style);
                        selectFocus.focus();
                        break;
                    case 'money':  
                        inputFocus.setAttribute("style", style);
                        inputFocus.focus();
                        break;
                    case 'autocomplete':
                        var htmlOption = '';
                        this.cellActived.dataInput.atribute.autoCompleteData.every(function(option, indexOption){
                            if(indexOption == 10) 
                                { return false; }

                            if(typeof option == 'string')
                                { htmlOption += '<option data-value="'+option+'" value="'+option+'"> </option> '; }
                            else
                                { htmlOption += '<option data-value="'+option.value+'" value="'+option.label+'"> </option> '; }

                            return true;
                        });
                        datalist.innerHTML = htmlOption;
                        inputFocus.setAttribute("list", 'datalist'+this.idJobbsOnTable);
                        inputFocus.setAttribute("style", style);
                        inputFocus.focus();
                    break;
                }
            },
            handleData: function handleData(arrDados){
                try{
                    var self = this;
                    var arrFinal = []; 
                    arrFinal = arrDados.map(function(line) {
                        if(self.maiorTr < line.length)
                            { self.maiorTr = line.length; }

                        line.map(function(atribute){

                            // put money mask
                            try{
                                if(atribute.atribute.type == 'money')
                                    { atribute.value = self.maskMoney(atribute.value); }
                            }catch(err){}

                            // put select value on td
                            try{
                               atribute.label = atribute.atribute.options.find(function (option) { return option.value == atribute.value }).label;
                            }catch(err){}

                            // put autocomplete value on td
                            try{
                                var autoCompleteOptionsAreText = typeof atribute.atribute.autoCompleteData[0] == 'string';
                                var newValue = atribute.atribute.autoCompleteData.find(function (option) { 
                                    if(autoCompleteOptionsAreText)
                                        { return option == atribute.value } 
                                    else
                                        { return option.value == atribute.value } 
                                });
                                atribute.label = autoCompleteOptionsAreText ? newValue : newValue.label;
                            }catch(err){}


                            // editando o style
                            if(typeof atribute == 'object'){
                                var style = ' ';

                                try{
                                    if(atribute.style.backgroundColor)
                                        { style += ' background-color: '+atribute.style.backgroundColor+'; '; }
                                }catch(err){}
                                
                                try{
                                    if(atribute.style.color)
                                        { style += ' color: '+atribute.style.color+'; '; }
                                }catch(err){}
                                    
                                try{
                                    if(atribute.style.fontSize)
                                        { style += ' font-size: '+atribute.style.fontSize+'px; '; }
                                }catch(err){}
                                    
                                try{
                                    if(atribute.style.fontFamily)
                                        { style += ' font-family: "'+atribute.style.fontFamily+'"; '; }
                                }catch(err){}
                                    
                                try{
                                    if(atribute.style.border)
                                        { style += ' border: '+atribute.style.border+'; '; }
                                }catch(err){}

                                try{
                                    if(atribute.style.size)
                                        { style += ' min-width: '+atribute.style.size+'; '; }
                                }catch(err){}

                                try{
                                    if(atribute.style.fontWeight)
                                        { style += ' font-weight: '+atribute.style.fontWeight+'; '; }
                                }catch(err){}

                                try{
                                    if(atribute.atribute.type == 'hidden'){
                                        style += ' display: none; ';
                                        self.maiorTr -= 1;
                                    }
                                }catch(err){}

                                try{ 
                                    if(atribute.atribute.disabled){
                                        style += ' color: #8c8c8c; ';
                                    }
                                }catch(err){}

                                atribute.style = style;
                            }
                            return atribute;
                        });
                        return line;
                    });
                    return arrFinal;
                }catch(err){}
                return [];
            },
            onInput: function onInput(){
                var atribute    = this.cellActived.dataInput;
                var indexLine   = this.cellActived.indexLine;
                var indexColumn = this.cellActived.indexColumn;

                // nao disparando evento ao focar na célula
                if(!this.triggerInput){ this.triggerInput = true; return false; }

                // nao permitindo alterar campo desabilitado
                if(atribute.atribute && atribute.atribute.disabled){ this.inputValue = this.inputValue.substring(0, this.inputValue.length-1); return false; }

                if(atribute.atribute.type == 'select'){
                    var self = this;
                    var valor = document.getElementById("selectCellFocus"+this.idJobbsOnTable).value;
                    atribute.value = valor;
                    this.inputValue = valor;
                    setTimeout(function(){ 
                        self.cellActived.eventInput.innerHTML = atribute.atribute.options.find(function (option) { return option.value == valor }).label;
                    }, 10);
                } else {
                    if(this.cellActived.eventInput.innerHTML != this.inputValue && !this.enterIsPress && atribute.atribute.type != 'autocomplete'){
                        var increasedWords  = this.inputValue.length > this.cellActived.eventInput.innerHTML.length;
                        var newValue        = increasedWords ? this.inputValue.substring(this.inputValue.length-1) : '';
                        this.cellActived.eventInput.innerHTML    = newValue;
                        this.value[indexLine][indexColumn].value = newValue;
                        this.inputValue     = newValue;
                        if(atribute.atribute.type == 'money')
                            { this.inputValue = this.maskMoney(newValue); }
                        this.enterIsPress   = true;
                    } else {
                        if(atribute.atribute.type == 'money')
                            { this.inputValue = this.maskMoney(this.inputValue); }
                        this.value[indexLine][indexColumn].value = this.inputValue;
                    }
                }

                // RESIZE THE CELL
                this.adjustField();

                // TRIGGER EVENTS
                if(typeof atribute.event == 'object' && typeof atribute.event.onChange == 'function'){ 
                    return atribute.event.onChange(atribute, indexLine, indexColumn, this.emitOnkey(indexLine, undefined, 'lineBefore'));
                } else {
                    return function () {};
                }
            },
            onInputEnterPressed: function onInputEnterPressed(){
                try{
                    if(this.cellActived.dataInput.atribute.type == 'autocomplete'){
                        this.moveCell('down');
                    }
                }catch(err){}

                if(this.enterIsPress){
                    this.enterIsPress = false;
                    this.moveCell('down');
                }else{
                    this.enterIsPress = true;    
                }
            },
            moveCell: function moveCell(moviment){
                this.cellActived.lastMoviment = moviment;
                var atribute    = this.cellActived.dataInput;
                var indexLine   = this.cellActived.indexLine;
                var indexColumn = this.cellActived.indexColumn;
                this.areAmoviment = true;

                switch(moviment.toUpperCase()){
                    case 'UP':
                        this.moveCellToUp();
                        break;
                    case 'DOWN':
                        this.moveCellToDown();
                        break;
                    case 'LEFT':
                        this.moveCellToLeft();
                        break;
                    case 'RIGHT':
                        this.moveCellToRight();
                        break;
                }

                // TRIGGER EVENTS
                if(typeof atribute.event == 'object' && typeof atribute.event.onFocusDown == 'function'){
                    return atribute.event.onFocusDown(atribute, indexLine, indexColumn, this.emitOnkey(indexLine, undefined, 'lineBefore'));
                } else {
                    return function () {};
                }
            },
            moveCellToUp: function moveCellToUp(){
                try{
                    var line     = this.cellActived.eventInput.parentElement;
                    var prevLine = line.previousElementSibling;
                    var index    = [].indexOf.call(line.children, this.cellActived.eventInput);
                    if(prevLine){
                        prevLine.children[index].click();
                    }
                }catch(err){
                }
            },
            moveCellToDown: function moveCellToDown(){
                try{
                    if(this.cellActived.dataInput.atribute.type != 'autocomplete'){
                        var line     = this.cellActived.eventInput.parentElement;
                        var index    = [].indexOf.call(line.children, this.cellActived.eventInput);
                        var nextLine = line.nextElementSibling;
                        if(nextLine)
                            { nextLine.children[index].click(); }
                    }
                }catch(err){
                }
            },
            moveCellToLeft: function moveCellToLeft(){
                try{
                    if(this.enterIsPress){
                        if(document.getElementById("inputCellFocus"+this.idJobbsOnTable).selectionStart == 0){
                            this.enterIsPress = false;
                            this.moveCell('left');
                        }
                    } else {
                        var prevElement = this.cellActived.eventInput.previousElementSibling;
                        var celulaEstaOculta = prevElement.style.display == 'none' && prevElement.tagName == 'TD';                        
                        if(prevElement != null && !celulaEstaOculta){
                            prevElement.click();
                        }
                    }
                }catch(err){
                }
            },
            moveCellToRight: function moveCellToRight(){
                try{
                    if(this.enterIsPress){
                        if(document.getElementById("inputCellFocus"+this.idJobbsOnTable).selectionStart == this.cellActived.dataInput.value.length){
                            this.enterIsPress = false;
                            this.moveCell('right');
                        }
                    }else{
                        var nextElement = this.cellActived.eventInput.nextElementSibling;
                        var celulaEstaOculta = nextElement.style.display == 'none' && nextElement.tagName == 'TD';
                        if(nextElement != null && !celulaEstaOculta){
                            nextElement.click();
                        }
                    }
                }catch(err){
                }
            },
            emitOnkey: function emitOnkey(lineBefore, lineAfter, type){
                if ( type === void 0 ) type = null;

                var arrLineBefore = [], arrLineAfter = [];

                try{
                    for(var i = 0 ; this.value[lineBefore][i] != undefined || this.value[lineBefore][i+1] != undefined ; i++){
                        try{

                            var valor = this.value[lineBefore][i].value;
                            if(this.value[lineBefore][i].atribute.type == 'money'){
                                valor = valor.replace(".", "").replace(",", ".");
                            }

                            arrLineBefore.push({
                                key : this.value[lineBefore][i].atribute.name,
                                value : valor,
                                indexLine : lineBefore,
                                indexColumn : i,
                            });
                        }catch(err){}
                    }
                }catch(err){}

                try{
                    for(var i = 0 ; eval("this.$refs.tdl"+(lineAfter)+"c"+(i)+" != undefined || this.$refs.tdl"+(lineAfter)+"c"+(i+1)+" != undefined") ; i++){
                        try{
                            eval("arrLineAfter.push( {"+
                                    " key: this.$refs.tdl"+(lineAfter)+"c"+i+"[0].getAttribute('name'),"+
                                    " value: this.$refs.tdl"+(lineAfter)+"c"+i+"[0].value, "+
                                    " refName: 'tdl"+(lineAfter)+"c"+(i)+"'"+
                                    "} );");
                        }catch(err){}
                    }
                }catch(err){}

                switch(type){
                    case 'up':
                        var retorno = {
                            lineBefore: arrLineBefore,
                            lineAfter:  arrLineAfter
                        };
                        this.$emit('onkeyup', retorno);
                        return retorno; 
                        break;
                    case 'down':
                        var retorno = {
                            lineBefore: arrLineBefore,
                            lineAfter:  arrLineAfter
                        };
                        this.$emit('onFocusDown', retorno);
                        return retorno;
                        break;
                    case 'click':
                        var retorno = {
                            lineBefore: arrLineBefore,
                            lineAfter:  arrLineAfter
                        };
                        this.$emit('onClickLineChange', retorno);
                        return retorno;
                        break;
                    case 'lineBefore': 
                        return {
                            lineBefore: arrLineBefore
                        }
                        break;
                    default:
                        return {
                            lineBefore: arrLineBefore,
                            lineAfter:  arrLineAfter
                        }
                        break;
                }
            },
            clickOnInput: function clickOnInput(line, column){
                this.enterIsPress = false;
                if(this.lineBefore != line){
                    if(this.lineBefore != null)
                        { this.emitOnkey(this.lineBefore, line, 'click'); }
                    this.lineBefore = line;
                }
            },
            setValueOnCell: function setValueOnCell(indexLine, indexColumn, value){
                try{
                    var input;
                    eval("input = this.$refs.tdl"+indexLine+"c"+indexColumn+"[0];");
                    if(input.hasAttribute("money"))
                        { value = this.maskMoney((value).toFixed(2)); }

                    input.innerHTML = value;
                }catch(err){}
            },
            maskMoney: function maskMoney(i) {
                try{
                    var v = String(i).replace(/\D/g,'');
                    v = (v/100).toFixed(2) + '';
                    v = v.replace(".", ",");
                    v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                    v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                    return v;
                }catch(err){
                    return i;
                }
            },
        },
        watch: {
            value: function () {
                this.dataWatch = this.handleData(this.value);
            }
        },
        mounted: function mounted() {
            this.dataWatch = this.handleData(this.value);            
        }
    };

    function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier
    /* server only */
    , shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
      if (typeof shadowMode !== 'boolean') {
        createInjectorSSR = createInjector;
        createInjector = shadowMode;
        shadowMode = false;
      } // Vue.extend constructor export interop.


      var options = typeof script === 'function' ? script.options : script; // render functions

      if (template && template.render) {
        options.render = template.render;
        options.staticRenderFns = template.staticRenderFns;
        options._compiled = true; // functional template

        if (isFunctionalTemplate) {
          options.functional = true;
        }
      } // scopedId


      if (scopeId) {
        options._scopeId = scopeId;
      }

      var hook;

      if (moduleIdentifier) {
        // server build
        hook = function hook(context) {
          // 2.3 injection
          context = context || // cached call
          this.$vnode && this.$vnode.ssrContext || // stateful
          this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext; // functional
          // 2.2 with runInNewContext: true

          if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
            context = __VUE_SSR_CONTEXT__;
          } // inject component styles


          if (style) {
            style.call(this, createInjectorSSR(context));
          } // register component module identifier for async chunk inference


          if (context && context._registeredComponents) {
            context._registeredComponents.add(moduleIdentifier);
          }
        }; // used by ssr in case component is cached and beforeCreate
        // never gets called


        options._ssrRegister = hook;
      } else if (style) {
        hook = shadowMode ? function () {
          style.call(this, createInjectorShadow(this.$root.$options.shadowRoot));
        } : function (context) {
          style.call(this, createInjector(context));
        };
      }

      if (hook) {
        if (options.functional) {
          // register for functional component in vue file
          var originalRender = options.render;

          options.render = function renderWithStyleInjection(h, context) {
            hook.call(context);
            return originalRender(h, context);
          };
        } else {
          // inject component registration as beforeCreate hook
          var existing = options.beforeCreate;
          options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
      }

      return script;
    }

    var normalizeComponent_1 = normalizeComponent;

    var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
    function createInjector(context) {
      return function (id, style) {
        return addStyle(id, style);
      };
    }
    var HEAD;
    var styles = {};

    function addStyle(id, css) {
      var group = isOldIE ? css.media || 'default' : id;
      var style = styles[group] || (styles[group] = {
        ids: new Set(),
        styles: []
      });

      if (!style.ids.has(id)) {
        style.ids.add(id);
        var code = css.source;

        if (css.map) {
          // https://developer.chrome.com/devtools/docs/javascript-debugging
          // this makes source maps inside style tags work properly in Chrome
          code += '\n/*# sourceURL=' + css.map.sources[0] + ' */'; // http://stackoverflow.com/a/26603875

          code += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) + ' */';
        }

        if (!style.element) {
          style.element = document.createElement('style');
          style.element.type = 'text/css';
          if (css.media) { style.element.setAttribute('media', css.media); }

          if (HEAD === undefined) {
            HEAD = document.head || document.getElementsByTagName('head')[0];
          }

          HEAD.appendChild(style.element);
        }

        if ('styleSheet' in style.element) {
          style.styles.push(code);
          style.element.styleSheet.cssText = style.styles.filter(Boolean).join('\n');
        } else {
          var index = style.ids.size - 1;
          var textNode = document.createTextNode(code);
          var nodes = style.element.childNodes;
          if (nodes[index]) { style.element.removeChild(nodes[index]); }
          if (nodes.length) { style.element.insertBefore(textNode, nodes[index]); }else { style.element.appendChild(textNode); }
        }
      }
    }

    var browser = createInjector;

    /* script */
    var __vue_script__ = script;

    /* template */
    var __vue_render__ = function() {
      var _vm = this;
      var _h = _vm.$createElement;
      var _c = _vm._self._c || _h;
      return _c("div", { staticClass: "row" }, [
        _c(
          "table",
          {
            class: "col-12 jobbsOnTable tableId" + _vm.idJobbsOnTable,
            attrs: { border: "1", bordercolor: "#eaeaea" }
          },
          [
            _c(
              "draggable",
              _vm._b(
                {
                  attrs: { tag: "tbody" },
                  on: { start: _vm.dragEvent, end: _vm.dropEvent },
                  model: {
                    value: _vm.dataWatch,
                    callback: function($$v) {
                      _vm.dataWatch = $$v;
                    },
                    expression: "dataWatch"
                  }
                },
                "draggable",
                _vm.dragOptions,
                false
              ),
              _vm._l(_vm.dataWatch, function(line, indexLine) {
                return _c(
                  "tr",
                  { key: indexLine, attrs: { id: indexLine } },
                  _vm._l(line, function(atribute, indexColumn) {
                    return _c("td", {
                      key: atribute.id,
                      ref: "tdl" + indexLine + "c" + indexColumn,
                      refInFor: true,
                      class: typeof atribute == "object" ? atribute.className : "",
                      style: atribute.style,
                      attrs: {
                        id: indexColumn,
                        colspan: _vm.maiorTr - line.length + 1
                      },
                      domProps: {
                        innerHTML: _vm._s(
                          typeof atribute == "string"
                            ? atribute
                            : atribute.label
                            ? atribute.label
                            : atribute.value
                        )
                      },
                      on: {
                        click: function($event) {
                          return _vm.onCellFocus(
                            $event,
                            atribute,
                            indexLine,
                            indexColumn
                          )
                        }
                      }
                    })
                  }),
                  0
                )
              }),
              0
            )
          ],
          1
        ),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "mask",
              rawName: "v-mask",
              value: _vm.mascaraInput,
              expression: "mascaraInput"
            },
            {
              name: "model",
              rawName: "v-model",
              value: _vm.inputValue,
              expression: "inputValue"
            }
          ],
          class: _vm.enterIsPress ? "cellPressed" : "",
          style: _vm.styleFocus,
          attrs: {
            type: "text",
            id: "inputCellFocus" + _vm.idJobbsOnTable,
            list: ""
          },
          domProps: { value: _vm.inputValue },
          on: {
            keyup: [
              function($event) {
                if (
                  !$event.type.indexOf("key") &&
                  _vm._k($event.keyCode, "left", 37, $event.key, [
                    "Left",
                    "ArrowLeft"
                  ])
                ) {
                  return null
                }
                if ("button" in $event && $event.button !== 0) {
                  return null
                }
                return _vm.moveCell("left")
              },
              function($event) {
                if (
                  !$event.type.indexOf("key") &&
                  _vm._k($event.keyCode, "up", 38, $event.key, ["Up", "ArrowUp"])
                ) {
                  return null
                }
                return _vm.moveCell("up")
              },
              function($event) {
                if (
                  !$event.type.indexOf("key") &&
                  _vm._k($event.keyCode, "down", 40, $event.key, [
                    "Down",
                    "ArrowDown"
                  ])
                ) {
                  return null
                }
                return _vm.moveCell("down")
              },
              function($event) {
                if (
                  !$event.type.indexOf("key") &&
                  _vm._k($event.keyCode, "right", 39, $event.key, [
                    "Right",
                    "ArrowRight"
                  ])
                ) {
                  return null
                }
                if ("button" in $event && $event.button !== 2) {
                  return null
                }
                return _vm.moveCell("right")
              },
              function($event) {
                if (
                  !$event.type.indexOf("key") &&
                  _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")
                ) {
                  return null
                }
                return _vm.onInputEnterPressed($event)
              }
            ],
            dblclick: function($event) {
    (_vm.enterIsPress = true),
                $event.target.setSelectionRange(
                  $event.target.value.length,
                  $event.target.value.length
                );
            },
            click: _vm.clickOnInput,
            input: [
              function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.inputValue = $event.target.value;
              },
              _vm.onInput
            ]
          }
        }),
        _vm._v(" "),
        _c("datalist", { attrs: { id: "datalist" + _vm.idJobbsOnTable } }),
        _vm._v(" "),
        _c("select", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.inputValue,
              expression: "inputValue"
            }
          ],
          style: _vm.styleFocus,
          attrs: { name: "", id: "selectCellFocus" + _vm.idJobbsOnTable },
          on: {
            keyup: [
              function($event) {
                if (
                  !$event.type.indexOf("key") &&
                  _vm._k($event.keyCode, "left", 37, $event.key, [
                    "Left",
                    "ArrowLeft"
                  ])
                ) {
                  return null
                }
                if ("button" in $event && $event.button !== 0) {
                  return null
                }
                return _vm.moveCell("left")
              },
              function($event) {
                if (
                  !$event.type.indexOf("key") &&
                  _vm._k($event.keyCode, "up", 38, $event.key, ["Up", "ArrowUp"])
                ) {
                  return null
                }
                return _vm.moveCell("up")
              },
              function($event) {
                if (
                  !$event.type.indexOf("key") &&
                  _vm._k($event.keyCode, "down", 40, $event.key, [
                    "Down",
                    "ArrowDown"
                  ])
                ) {
                  return null
                }
                return _vm.moveCell("down")
              },
              function($event) {
                if (
                  !$event.type.indexOf("key") &&
                  _vm._k($event.keyCode, "right", 39, $event.key, [
                    "Right",
                    "ArrowRight"
                  ])
                ) {
                  return null
                }
                if ("button" in $event && $event.button !== 2) {
                  return null
                }
                return _vm.moveCell("right")
              },
              function($event) {
                if (
                  !$event.type.indexOf("key") &&
                  _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")
                ) {
                  return null
                }
                return _vm.moveCell("down")
              }
            ],
            click: _vm.clickOnInput,
            input: _vm.onInput,
            change: function($event) {
              var $$selectedVal = Array.prototype.filter
                .call($event.target.options, function(o) {
                  return o.selected
                })
                .map(function(o) {
                  var val = "_value" in o ? o._value : o.value;
                  return val
                });
              _vm.inputValue = $event.target.multiple
                ? $$selectedVal
                : $$selectedVal[0];
            }
          }
        })
      ])
    };
    var __vue_staticRenderFns__ = [];
    __vue_render__._withStripped = true;

      /* style */
      var __vue_inject_styles__ = function (inject) {
        if (!inject) { return }
        inject("data-v-53c28a30_0", { source: "\n.hiddenCellFocus[data-v-53c28a30]:focus {\n    color: transparent !important;\n    text-shadow: 0 0 0 #8c8c8c;\n    outline: none;\n}\n.jobbsOnTable select[data-v-53c28a30]{\n    color:black !important;\n}\ninput[data-v-53c28a30]:focus, select[data-v-53c28a30]:focus  {\n    color: transparent ;\n    text-shadow: 0 0 0 black;\n    outline: none;\n\n    border: 0px solid #636363 ;\n    box-shadow: 0 0 3px #636363 ;\n    -moz-box-shadow: 0 0 3px #636363 ;\n    -webkit-box-shadow: 0 0 3px #636363 ;\n}\n.cellPressed[data-v-53c28a30] {\n    color: black !important;\n    text-shadow: 0 0 0 black !important;\n    outline: black !important;\n    border-radius: 3px !important;\n    border: 0px solid #17a2b8 !important;\n    box-shadow: 0 0 6px #17a2b8 !important;\n    -moz-box-shadow: 0 0 6px #17a2b8 !important;\n    -webkit-box-shadow: 0 0 6px #17a2b8 !important;\n}\n.jobbsOnTable .flip-list-move[data-v-53c28a30] {\n    transition: transform 0.5s;\n}\n.jobbsOnTable .no-move[data-v-53c28a30] {\n    transition: transform 0s;\n}\n.jobbsOnTable .ghost[data-v-53c28a30] {\n    opacity: 0.5;\n    background: #c8ebfb;\n}\n.menu-box[data-v-53c28a30]{\n    padding: 1px 10px;\n    width: auto !important;\n}\n", map: {"version":3,"sources":["/Users/JoaoNeto/Documents/JOBB_NOVO/jobbsontable/src/jobbsontable.vue"],"names":[],"mappings":";AAonBA;IACA,6BAAA;IACA,0BAAA;IACA,aAAA;AACA;AAEA;IACA,sBAAA;AACA;AAEA;IACA,mBAAA;IACA,wBAAA;IACA,aAAA;;IAEA,0BAAA;IACA,4BAAA;IACA,iCAAA;IACA,oCAAA;AACA;AACA;IACA,uBAAA;IACA,mCAAA;IACA,yBAAA;IACA,6BAAA;IACA,oCAAA;IACA,sCAAA;IACA,2CAAA;IACA,8CAAA;AACA;AACA;IACA,0BAAA;AACA;AACA;IACA,wBAAA;AACA;AACA;IACA,YAAA;IACA,mBAAA;AACA;AACA;IACA,iBAAA;IACA,sBAAA;AACA","file":"jobbsontable.vue","sourcesContent":["<template>\n    <div class=\"row\">\n        <table \n            border=\"1\" \n            bordercolor=\"#eaeaea\" \n            :class=\"'col-12 jobbsOnTable tableId'+idJobbsOnTable \"\n            >\n            <draggable \n                v-model=\"dataWatch\" \n                v-bind=\"dragOptions\"\n                tag=\"tbody\" \n                @start=\"dragEvent\"\n                @end=\"dropEvent\">\n                    <tr \n                        v-for=\"(line, indexLine) in dataWatch\" \n                        :key=\"indexLine\"\n                        :id=\"indexLine\"\n                    >\n                        <td v-for=\"(atribute, indexColumn) in line\" \n                            :key=\"atribute.id\"\n                            :id=\"indexColumn\"\n                            :colspan=\"maiorTr - line.length + 1\"\n                            :class=\"typeof atribute == 'object' ? atribute.className : ''\"\n                            :style=\"atribute.style\"\n                            @click=\"onCellFocus($event, atribute, indexLine, indexColumn)\"\n                            v-html=\"typeof atribute == 'string' ? atribute : (atribute.label ? atribute.label : atribute.value)\"\n                            :ref=\"'tdl'+indexLine+'c'+indexColumn\"\n                        ></td>\n                    </tr>\n            </draggable>\n        </table>\n\n        <input \n            type=\"text\" \n            :id=\"'inputCellFocus'+idJobbsOnTable\" \n            :style=\"styleFocus\"\n            v-mask=\"mascaraInput\"\n            v-model=\"inputValue\"\n            list=\"\" \n            :class=\"enterIsPress ? 'cellPressed' : ''\"\n\n            @keyup.left =\"moveCell('left')\"\n            @keyup.up   =\"moveCell('up')\"\n            @keyup.down =\"moveCell('down')\"\n            @keyup.right=\"moveCell('right')\"\n            @keyup.enter=\"onInputEnterPressed\"\n            @dblclick=\"enterIsPress = true, $event.target.setSelectionRange($event.target.value.length, $event.target.value.length)\"\n            @click=\"clickOnInput\"\n            @input=\"onInput\"\n        >\n        <datalist :id=\"'datalist'+idJobbsOnTable\"></datalist>\n\n        <select \n            name=\"\" \n            :id=\"'selectCellFocus'+idJobbsOnTable\"\n            :style=\"styleFocus\"\n            v-model=\"inputValue\"\n\n            @keyup.left =\"moveCell('left')\"\n            @keyup.up   =\"moveCell('up')\"\n            @keyup.down =\"moveCell('down')\"\n            @keyup.right=\"moveCell('right')\"\n            @keyup.enter=\"moveCell('down')\"\n            @click=\"clickOnInput\"\n            @input=\"onInput\"\n        ></select>\n\n    </div>\n</template>\n<script>\n    import draggable from 'vuedraggable'\n    import { VueMaskDirective } from 'v-mask'\n\n    export default {\n        props: {\n            value: {\n                type: [Array, Object],\n                required: true\n            },\n            dragAndDrop:{\n                type: Boolean,\n                required: false,\n                default: false\n            },\n            noDragAndDropByClassName:{\n                type: String,\n                required: false,\n                default: ''\n            },\n            dragEvent: {\n                type: Function,\n                required: false,\n                default: () => {}\n            },\n            dropEvent: {\n                type: Function,\n                required: false,\n                default: () => {}\n            },\n            idJobbsOnTable: {\n                type: String,\n                default: function () {\n                    return Math.random().toString().substr(-Math.random().toString()+3);\n                } \n            },\n            styleFocus: {\n                type: String,\n                default: 'box-sizing:border-box; display:none; position:absolute;'\n            }\n        },      \n        directives: {\n            'mask' : VueMaskDirective\n        },\n        components: {\n            draggable\n        },\n        computed: {\n            dragOptions() {\n                return {\n                    animation: 200,\n                    ghostClass: \"ghost\",\n                    easing: \"cubic-bezier(0.19, 1, 0.22, 1)\",\n                    filter: this.noDragAndDropByClassName,\n                    disabled: !this.dragAndDrop\n                };\n            },\n        },\n        data() {\n            return {\n                inputValue: '',\n                mascaraInput: '',\n                dataWatch: [],\n                maiorTr: 0,\n                enterIsPress: false,\n                areAmoviment: false,\n                triggerInput: false,\n                lineBefore: null,\n                cellActived: {\n                    indexColumn: null,\n                    indexLine: null,\n                    eventInput: null,\n                    dataInput: null,\n                    lastMoviment: null\n                }\n            }\n        },\n        methods: {\n            onCellFocus(event, campo, indexLine, indexColumn){\n                try{\n\n                    let self = this;\n                    this.enterIsPress = false;\n                    this.cellActived.eventInput  = event.target;\n                    this.cellActived.indexColumn = indexColumn;\n                    this.cellActived.indexLine   = indexLine;\n                    this.cellActived.dataInput   = campo;\n                    this.triggerInput            = false;\n                    this.inputValue              = null;\n\n                    if(!this.areAmoviment){\n                        if(typeof this.cellActived.dataInput.event == 'object' && typeof this.cellActived.dataInput.event.onClick == 'function')\n                            this.cellActived.dataInput.event.onClick(campo, indexLine, indexColumn); // TRIGGER EVENT CLICK\n                    }\n                    this.areAmoviment = false;\n\n                    this.adjustField();\n\n                    switch(campo.atribute.type){\n                        case 'input':  \n                            this.inputValue = event.target.innerHTML;\n                            setTimeout(function(){ self.inputValue = event.target.innerHTML }, 30);\n                            break;\n                        case 'select': \n                            let htmlOption = '';\n                            let selectFocus = document.getElementById(\"selectCellFocus\"+this.idJobbsOnTable);\n                            campo.atribute.options.forEach(function(option){ \n                                htmlOption += '<option value=\"'+option.value+'\" >'+option.label+'</option>'; \n                            });\n\n                            selectFocus.innerHTML = htmlOption;\n                            this.inputValue       = campo.value; \n                            document.getElementById(\"selectCellFocus\"+this.idJobbsOnTable).value = campo.value; \n\n                            break;\n                        case 'money':\n                            this.inputValue = event.target.innerHTML;\n                            break;\n                        case 'autocomplete':\n                            this.inputValue = event.target.innerHTML;\n                            break;\n                    }\n\n                }catch(err){\n                    if(this.cellActived.lastMoviment != null){\n                        this.moveCell(this.cellActived.lastMoviment);\n                        this.cellActived.lastMoviment = null;\n                    }\n                }\n            },\n            adjustField(){\n                const tableJobbsOnTable = document.getElementsByClassName(\"tableId\"+this.idJobbsOnTable)[0];\n                const inputFocus        = document.getElementById(\"inputCellFocus\"+this.idJobbsOnTable);\n                const datalist          = document.getElementById(\"datalist\"+this.idJobbsOnTable);\n                const selectFocus       = document.getElementById(\"selectCellFocus\"+this.idJobbsOnTable);\n                inputFocus.setAttribute(\"list\", '');\n                inputFocus.setAttribute(\"style\", this.styleFocus);\n                selectFocus.setAttribute(\"style\", this.styleFocus);\n\n                // DISABLED \n                try{ \n                    if(this.cellActived.dataInput.atribute.disabled){\n                        inputFocus.classList.add(\"hiddenCellFocus\");\n                        selectFocus.classList.add(\"hiddenCellFocus\");\n                    } else {\n                        throw true; \n                    }\n                }catch(err){\n                    inputFocus.classList.remove(\"hiddenCellFocus\");\n                    selectFocus.classList.remove(\"hiddenCellFocus\");\n                }\n\n                // PUT THE MASK\n                try{\n                    if(this.cellActived.dataInput.atribute.mask.length > 0){\n                        this.mascaraInput = this.cellActived.dataInput.atribute.mask;\n                    }else{\n                        throw true; \n                    }\n                }catch(err){\n                    this.mascaraInput = '';\n                }\n\n                // RESIZE\n                var position = this.cellActived.eventInput.getBoundingClientRect();\n                var left     = tableJobbsOnTable.offsetLeft + this.cellActived.eventInput.offsetLeft;\n                var top      = tableJobbsOnTable.offsetTop + this.cellActived.eventInput.offsetTop;\n                let style    = \"box-sizing: border-box; display: inline-block; position: absolute; top: \"+top+\"px; left:\"+left+\"px; width: \"+position.width+\"px; height:\"+position.height+\"px;\"+this.cellActived.dataInput.style;\n\n                switch(this.cellActived.dataInput.atribute.type){\n                    case 'input':  \n                        inputFocus.setAttribute(\"style\", style);\n                        inputFocus.focus();\n                        break;\n                    case 'select': \n                        selectFocus.setAttribute(\"style\", style);\n                        selectFocus.focus();\n                        break;\n                    case 'money':  \n                        inputFocus.setAttribute(\"style\", style);\n                        inputFocus.focus();\n                        break;\n                    case 'autocomplete':\n                        let htmlOption = '';\n                        this.cellActived.dataInput.atribute.autoCompleteData.every(function(option, indexOption){\n                            if(indexOption == 10) \n                                return false;\n\n                            if(typeof option == 'string')\n                                htmlOption += '<option data-value=\"'+option+'\" value=\"'+option+'\"> </option> ';\n                            else\n                                htmlOption += '<option data-value=\"'+option.value+'\" value=\"'+option.label+'\"> </option> ';\n\n                            return true;\n                        });\n                        datalist.innerHTML = htmlOption;\n                        inputFocus.setAttribute(\"list\", 'datalist'+this.idJobbsOnTable);\n                        inputFocus.setAttribute(\"style\", style);\n                        inputFocus.focus();\n                    break;\n                }\n            },\n            handleData(arrDados){\n                try{\n                    let self = this;\n                    let arrFinal = []; \n                    arrFinal = arrDados.map(function(line) {\n                        if(self.maiorTr < line.length)\n                            self.maiorTr = line.length;\n\n                        line.map(function(atribute){\n\n                            // put money mask\n                            try{\n                                if(atribute.atribute.type == 'money')\n                                    atribute.value = self.maskMoney(atribute.value);\n                            }catch(err){}\n\n                            // put select value on td\n                            try{\n                               atribute.label = atribute.atribute.options.find((option) => { return option.value == atribute.value }).label;\n                            }catch(err){}\n\n                            // put autocomplete value on td\n                            try{\n                                let autoCompleteOptionsAreText = typeof atribute.atribute.autoCompleteData[0] == 'string';\n                                let newValue = atribute.atribute.autoCompleteData.find((option) => { \n                                    if(autoCompleteOptionsAreText)\n                                        return option == atribute.value \n                                    else\n                                        return option.value == atribute.value \n                                });\n                                atribute.label = autoCompleteOptionsAreText ? newValue : newValue.label\n                            }catch(err){}\n\n\n                            // editando o style\n                            if(typeof atribute == 'object'){\n                                var style = ' ';\n\n                                try{\n                                    if(atribute.style.backgroundColor)\n                                        style += ' background-color: '+atribute.style.backgroundColor+'; ';\n                                }catch(err){}\n                                \n                                try{\n                                    if(atribute.style.color)\n                                        style += ' color: '+atribute.style.color+'; ';\n                                }catch(err){}\n                                    \n                                try{\n                                    if(atribute.style.fontSize)\n                                        style += ' font-size: '+atribute.style.fontSize+'px; ';\n                                }catch(err){}\n                                    \n                                try{\n                                    if(atribute.style.fontFamily)\n                                        style += ' font-family: \"'+atribute.style.fontFamily+'\"; ';\n                                }catch(err){}\n                                    \n                                try{\n                                    if(atribute.style.border)\n                                        style += ' border: '+atribute.style.border+'; ';\n                                }catch(err){}\n\n                                try{\n                                    if(atribute.style.size)\n                                        style += ' min-width: '+atribute.style.size+'; ';\n                                }catch(err){}\n\n                                try{\n                                    if(atribute.style.fontWeight)\n                                        style += ' font-weight: '+atribute.style.fontWeight+'; ';\n                                }catch(err){}\n\n                                try{\n                                    if(atribute.atribute.type == 'hidden'){\n                                        style += ' display: none; ';\n                                        self.maiorTr -= 1;\n                                    }\n                                }catch(err){}\n\n                                try{ \n                                    if(atribute.atribute.disabled){\n                                        style += ' color: #8c8c8c; ';\n                                    }\n                                }catch(err){}\n\n                                atribute.style = style;\n                            }\n                            return atribute;\n                        });\n                        return line;\n                    });\n                    return arrFinal;\n                }catch(err){}\n                return [];\n            },\n            onInput(){\n                let atribute    = this.cellActived.dataInput;\n                let indexLine   = this.cellActived.indexLine;\n                let indexColumn = this.cellActived.indexColumn;\n\n                // nao disparando evento ao focar na célula\n                if(!this.triggerInput){ this.triggerInput = true; return false; }\n\n                // nao permitindo alterar campo desabilitado\n                if(atribute.atribute && atribute.atribute.disabled){ this.inputValue = this.inputValue.substring(0, this.inputValue.length-1); return false; }\n\n                if(atribute.atribute.type == 'select'){\n                    let self = this;\n                    let valor = document.getElementById(\"selectCellFocus\"+this.idJobbsOnTable).value;\n                    atribute.value = valor;\n                    this.inputValue = valor;\n                    setTimeout(function(){ \n                        self.cellActived.eventInput.innerHTML = atribute.atribute.options.find((option) => { return option.value == valor }).label;\n                    }, 10);\n                } else {\n                    if(this.cellActived.eventInput.innerHTML != this.inputValue && !this.enterIsPress && atribute.atribute.type != 'autocomplete'){\n                        let increasedWords  = this.inputValue.length > this.cellActived.eventInput.innerHTML.length;\n                        var newValue        = increasedWords ? this.inputValue.substring(this.inputValue.length-1) : '';\n                        this.cellActived.eventInput.innerHTML    = newValue;\n                        this.value[indexLine][indexColumn].value = newValue;\n                        this.inputValue     = newValue;\n                        if(atribute.atribute.type == 'money')\n                            this.inputValue = this.maskMoney(newValue);\n                        this.enterIsPress   = true;\n                    } else {\n                        if(atribute.atribute.type == 'money')\n                            this.inputValue = this.maskMoney(this.inputValue);\n                        this.value[indexLine][indexColumn].value = this.inputValue;\n                    }\n                }\n\n                // RESIZE THE CELL\n                this.adjustField();\n\n                // TRIGGER EVENTS\n                if(typeof atribute.event == 'object' && typeof atribute.event.onChange == 'function'){ \n                    return atribute.event.onChange(atribute, indexLine, indexColumn, this.emitOnkey(indexLine, undefined, 'lineBefore'));\n                } else {\n                    return () => {};\n                }\n            },\n            onInputEnterPressed(){\n                try{\n                    if(this.cellActived.dataInput.atribute.type == 'autocomplete'){\n                        this.moveCell('down');\n                    }\n                }catch(err){}\n\n                if(this.enterIsPress){\n                    this.enterIsPress = false;\n                    this.moveCell('down');\n                }else{\n                    this.enterIsPress = true;    \n                }\n            },\n            moveCell(moviment){\n                this.cellActived.lastMoviment = moviment;\n                let atribute    = this.cellActived.dataInput;\n                let indexLine   = this.cellActived.indexLine;\n                let indexColumn = this.cellActived.indexColumn;\n                this.areAmoviment = true;\n\n                switch(moviment.toUpperCase()){\n                    case 'UP':\n                        this.moveCellToUp();\n                        break;\n                    case 'DOWN':\n                        this.moveCellToDown();\n                        break;\n                    case 'LEFT':\n                        this.moveCellToLeft();\n                        break;\n                    case 'RIGHT':\n                        this.moveCellToRight();\n                        break;\n                }\n\n                // TRIGGER EVENTS\n                if(typeof atribute.event == 'object' && typeof atribute.event.onFocusDown == 'function'){\n                    return atribute.event.onFocusDown(atribute, indexLine, indexColumn, this.emitOnkey(indexLine, undefined, 'lineBefore'));\n                } else {\n                    return () => {};\n                }\n            },\n            moveCellToUp(){\n                try{\n                    var line     = this.cellActived.eventInput.parentElement;\n                    var prevLine = line.previousElementSibling;\n                    var index    = [].indexOf.call(line.children, this.cellActived.eventInput);\n                    if(prevLine){\n                        prevLine.children[index].click();\n                    }\n                }catch(err){\n                }\n            },\n            moveCellToDown(){\n                try{\n                    if(this.cellActived.dataInput.atribute.type != 'autocomplete'){\n                        var line     = this.cellActived.eventInput.parentElement;\n                        var index    = [].indexOf.call(line.children, this.cellActived.eventInput);\n                        var nextLine = line.nextElementSibling;\n                        if(nextLine)\n                            nextLine.children[index].click();\n                    }\n                }catch(err){\n                }\n            },\n            moveCellToLeft(){\n                try{\n                    if(this.enterIsPress){\n                        if(document.getElementById(\"inputCellFocus\"+this.idJobbsOnTable).selectionStart == 0){\n                            this.enterIsPress = false;\n                            this.moveCell('left');\n                        }\n                    } else {\n                        const prevElement = this.cellActived.eventInput.previousElementSibling;\n                        const celulaEstaOculta = prevElement.style.display == 'none' && prevElement.tagName == 'TD';                        \n                        if(prevElement != null && !celulaEstaOculta){\n                            prevElement.click();\n                        }\n                    }\n                }catch(err){\n                }\n            },\n            moveCellToRight(){\n                try{\n                    if(this.enterIsPress){\n                        if(document.getElementById(\"inputCellFocus\"+this.idJobbsOnTable).selectionStart == this.cellActived.dataInput.value.length){\n                            this.enterIsPress = false;\n                            this.moveCell('right');\n                        }\n                    }else{\n                        const nextElement = this.cellActived.eventInput.nextElementSibling;\n                        const celulaEstaOculta = nextElement.style.display == 'none' && nextElement.tagName == 'TD';\n                        if(nextElement != null && !celulaEstaOculta){\n                            nextElement.click();\n                        }\n                    }\n                }catch(err){\n                }\n            },\n            emitOnkey(lineBefore, lineAfter, type = null){\n                var arrLineBefore = [], arrLineAfter = [];\n\n                try{\n                    for(var i = 0 ; this.value[lineBefore][i] != undefined || this.value[lineBefore][i+1] != undefined ; i++){\n                        try{\n\n                            let valor = this.value[lineBefore][i].value;\n                            if(this.value[lineBefore][i].atribute.type == 'money'){\n                                valor = valor.replace(\".\", \"\").replace(\",\", \".\");\n                            }\n\n                            arrLineBefore.push({\n                                key : this.value[lineBefore][i].atribute.name,\n                                value : valor,\n                                indexLine : lineBefore,\n                                indexColumn : i,\n                            });\n                        }catch(err){}\n                    }\n                }catch(err){}\n\n                try{\n                    for(var i = 0 ; eval(\"this.$refs.tdl\"+(lineAfter)+\"c\"+(i)+\" != undefined || this.$refs.tdl\"+(lineAfter)+\"c\"+(i+1)+\" != undefined\") ; i++){\n                        try{\n                            eval(\"arrLineAfter.push( {\"+\n                                    \" key: this.$refs.tdl\"+(lineAfter)+\"c\"+i+\"[0].getAttribute('name'),\"+\n                                    \" value: this.$refs.tdl\"+(lineAfter)+\"c\"+i+\"[0].value, \"+\n                                    \" refName: 'tdl\"+(lineAfter)+\"c\"+(i)+\"'\"+\n                                    \"} );\");\n                        }catch(err){}\n                    }\n                }catch(err){}\n\n                switch(type){\n                    case 'up':\n                        var retorno = {\n                            lineBefore: arrLineBefore,\n                            lineAfter:  arrLineAfter\n                        };\n                        this.$emit('onkeyup', retorno);\n                        return retorno; \n                        break;\n                    case 'down':\n                        var retorno = {\n                            lineBefore: arrLineBefore,\n                            lineAfter:  arrLineAfter\n                        };\n                        this.$emit('onFocusDown', retorno);\n                        return retorno;\n                        break;\n                    case 'click':\n                        var retorno = {\n                            lineBefore: arrLineBefore,\n                            lineAfter:  arrLineAfter\n                        };\n                        this.$emit('onClickLineChange', retorno);\n                        return retorno;\n                        break;\n                    case 'lineBefore': \n                        return {\n                            lineBefore: arrLineBefore\n                        }\n                        break;\n                    default:\n                        return {\n                            lineBefore: arrLineBefore,\n                            lineAfter:  arrLineAfter\n                        }\n                        break;\n                }\n            },\n            clickOnInput(line, column){\n                this.enterIsPress = false;\n                if(this.lineBefore != line){\n                    if(this.lineBefore != null)\n                        this.emitOnkey(this.lineBefore, line, 'click');\n                    this.lineBefore = line;\n                }\n            },\n            setValueOnCell(indexLine, indexColumn, value){\n                try{\n                    let input;\n                    eval(\"input = this.$refs.tdl\"+indexLine+\"c\"+indexColumn+\"[0];\");\n                    if(input.hasAttribute(\"money\"))\n                        value = this.maskMoney((value).toFixed(2));\n\n                    input.innerHTML = value;\n                }catch(err){}\n            },\n            maskMoney(i) {\n                try{\n                    var v = String(i).replace(/\\D/g,'');\n                    v = (v/100).toFixed(2) + '';\n                    v = v.replace(\".\", \",\");\n                    v = v.replace(/(\\d)(\\d{3})(\\d{3}),/g, \"$1.$2.$3,\");\n                    v = v.replace(/(\\d)(\\d{3}),/g, \"$1.$2,\");\n                    return v;\n                }catch(err){\n                    return i;\n                }\n            },\n        },\n        watch: {\n            value: function () {\n                this.dataWatch = this.handleData(this.value);\n            }\n        },\n        mounted() {\n            this.dataWatch = this.handleData(this.value);            \n        }\n    }\n</script>\n\n<style scoped>\n    .hiddenCellFocus:focus {\n        color: transparent !important;\n        text-shadow: 0 0 0 #8c8c8c;\n        outline: none;\n    }\n    \n    .jobbsOnTable select{\n        color:black !important;\n    }\n\n    input:focus, select:focus  {\n        color: transparent ;\n        text-shadow: 0 0 0 black;\n        outline: none;\n\n        border: 0px solid #636363 ;\n        box-shadow: 0 0 3px #636363 ;\n        -moz-box-shadow: 0 0 3px #636363 ;\n        -webkit-box-shadow: 0 0 3px #636363 ;\n    }\n    .cellPressed {\n        color: black !important;\n        text-shadow: 0 0 0 black !important;\n        outline: black !important;\n        border-radius: 3px !important;\n        border: 0px solid #17a2b8 !important;\n        box-shadow: 0 0 6px #17a2b8 !important;\n        -moz-box-shadow: 0 0 6px #17a2b8 !important;\n        -webkit-box-shadow: 0 0 6px #17a2b8 !important;\n    }\n    .jobbsOnTable .flip-list-move {\n        transition: transform 0.5s;\n    }\n    .jobbsOnTable .no-move {\n        transition: transform 0s;\n    }\n    .jobbsOnTable .ghost {\n        opacity: 0.5;\n        background: #c8ebfb;\n    }\n    .menu-box{\n        padding: 1px 10px;\n        width: auto !important;\n    }\n</style>\n"]}, media: undefined });

      };
      /* scoped */
      var __vue_scope_id__ = "data-v-53c28a30";
      /* module identifier */
      var __vue_module_identifier__ = undefined;
      /* functional template */
      var __vue_is_functional_template__ = false;
      /* style inject SSR */
      

      
      var component = normalizeComponent_1(
        { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
        __vue_inject_styles__,
        __vue_script__,
        __vue_scope_id__,
        __vue_is_functional_template__,
        __vue_module_identifier__,
        browser,
        undefined
      );

    // Importa o componente

    // Declara a função de instalação executada pelo Vue.use()
    function install(Vue) {
      if (install.installed) { return; }
      install.installed = true;
      Vue.component('jobbsontable', component);
    }

    // Cria a definição do módulo para Vue.use()
    var plugin = {
      install: install,
    };

    // Auto-instala quando o Vue é encontrado (no navegador via <script>)
    var GlobalVue = null;
    if (typeof window !== 'undefined') {
      GlobalVue = window.Vue;
    } else if (typeof global !== 'undefined') {
      GlobalVue = global.Vue;
    }
    if (GlobalVue) {
      GlobalVue.use(plugin);
    }

    exports.default = component;
    exports.install = install;

    Object.defineProperty(exports, '__esModule', { value: true });

}));
