## O dockerfile é o arquivo node que mostra o passo a passo do que fazer com seu projeto no docker, 
## ele funciona como uma receita de bolo, executando linha a linha dos comandos que voce pretende.

# Determinando que a imagem base vai ser a ultima versão do node
FROM node:latest

# Cria a pasta base do projeto
RUN mkdir -p /usr/src/app

# Definindo o diretório de trabalho desse container
WORKDIR /usr/src/app

# estou copiando o arquivo package.json da minha maquina e colocando no meu diretório de trabalho do container
COPY package*.json /usr/src/app/

# Executando o comando para instalar os pacotes no meu workdir do container
RUN npm install

# Copiando todos os arquivos do meu diretório local e colocando no workdir do container
COPY . /usr/src/app

# Definindo a porta que vai executar esse container
EXPOSE 8080

# Comando cmd que será executado pelo container (esse comando executa o projeto)
CMD ["npm", "run", "dev"]

# DOCKER BUILD -t <nome do container> . => Cria o container a partir das espesificações do Dockerfile
# docker run -p 8080:8080 -d   jobbsontablecontainer