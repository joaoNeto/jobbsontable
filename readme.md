### JOBBS ON TABLE 

Procurando um pacote que monte suas tabelas html dinamicamente? que se comporte como planilhas xls? com uma série de eventos, atributos e estilos que voce pode colocar nas suas células? eu acho que o jobbsontable é a sua solução, venha se aventurar e se viciar nesse package feito e pensado em vue.js para voce.

<b>[Veja o link de demonstração](http://jobbsontable.sistemajobb.com.br/)</b>

#### SOBRE O COMPONENTE

Para montar a tabela do jobbsontable voce precisa passar o seu json array no atributo <b>data</b> no padrão exigido pelo componente, voce pode consultar mais detalhes nas linhas abaixo, o componente também possui alguns métodos o <b>onkeydown</b> e <b>onkeyup</b>, voce pode navegar pelas células utilizando os direcionais do teclado ( o movimento só ocorre para inputs e selects) quando o usuário pressionar a tecla para baixo (down) ou para cima (up) ele dispara a sua função retornando o um objeto com dois atributos o <i>lineBefore</i> e o <i>lineAfter</i>, ambos também são objetos contendo todos os valores da linha anterior (lineBefore) ao click para cima ou para baixo e a linha atual (lineAfter) que está no foco.

#### USO BÁSICO

CÓDIGO HTML

```html
    <template>
        <div class="row">
            <jobbs-on-table v-model="arrData"></jobbs-on-table>
        </div>
    </template>
```
CÓDIGO VUE

```javascript

<script type="text/javascript">
    import jobbsOnTable from 'jobbsontable'
    export default {
        components: {
            jobbsOnTable
        },
        data() {
            return {
                arrData: [ 
                    [
                        {
                            value: '115.439.274-08',
                            className: '',
                            atribute: {
                                type: 'input', 
                                lenght: 11, 
                                mask: '###.###.###-##',
                                disabled: true
                            },
                            style: {
                                'backgroundColor': 'white',
                                'color': 'grey'
                            },
                            event: {
                                onClick: function(){},
                                onChange: function(){},
                                onkeyDown: function(){}
                            }
                        }
                    ],
                    [
                        {...},
                        {...}
                    ]
                ]
            }
        }
    }
</script>
```

#### ATRIBUTOS

São os atributos da célula, define a sua estrutura de exibição da célula. os tipos de atributos são.:

| nome atributo  | tipo | descricao | default |
|----------------|------|-----------|---------|
| type   | String  | tipo da célula, ex.: text, input, select, money, etc. | text |
| length   | Integer  | Tamanho do valor da célula | 200 |
| mask   | String  | Máscara do campo, ex.: ##/##/#### | null |
| options   | Array  | opções de escolha, <b>Apenas para type marcado como select</b> | null |
| disabled | Boolean | Desabilita o input ou select |
| name | String | Atributo name da input ou select |


#### ESTILO

Define um estilo css para a célula.
<br>
<b> obs.: voce também pode definir um nome de classe para a sua célula acresentando o campo <i>className</i> no objeto JSON da célula. </b>

| nome atributo | descrição |
|---------------|-----------|
|  backgroundColor | Cor de fundo da célula |
|  color | Cor do texto da célula |
|  fontSize | Tamanho da fonte da célula |
|  fontFamily | estilo de fonte da célula |
|  border | borda da célula, ex.: 4px solid black |
|  size | Tamanho da célula, ex.: 10px, 10% |

#### EVENTOS
os eventos são as ações que irão ocorrer quando o usuário interagir com as células da tabela

| nome evento | descrição | 
|-------------|-----------| 
|  onClick | Evento disparado quando clicar na célula |
|  onChange | Evento disparado ao modificar o valor da célula, estes eventos só são disparados para celulas com o tipo de atributo <b>input</b> ou <b>select</b> |
|  onkeyDown | Dispara o evento quando o mouse tirar o foco da célula |

#### DRAG AND DROP 

    <jobbs-on-table 
        :data="listarDadosItemOrcamento" 
        :dragAndDrop="true"
        noDragAndDropByClassName=".grupoSuperior, .grupoInferior, .menuItem"
        dragEvent=""
        dropEvent=""
    ></jobbs-on-table>
